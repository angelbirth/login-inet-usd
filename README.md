Aplikasi command-line sederhana untuk login ke internet kampus USD tanpa membuka browser

Instruksi
---------
```bash
git clone https://gitlab.com/angelbirth/login-inet-usd
cd login-inet-usd
./gradlew installDist #atau gradle installDist kalau punya distribusi gradle lokal
cd build/install/login-inet
bin/login-inet <nim> <password>
```

Download
--------
Distribusi dapat di-download di [sini](https://gitlab.com/angelbirth/login-inet-usd/builds/artifacts/master/download?job=dist).
Persyaratan: JDK versi 1.8