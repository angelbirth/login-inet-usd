import okhttp3.FormBody as Body
import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.commons.codec.digest.DigestUtils
import org.jsoup.Jsoup
import java.io.File
import java.io.FileInputStream
import java.net.URL
import java.util.*

/**
 * Created by ric on 07/02/17.
 */
fun request(closure: (Request.Builder.() -> Request.Builder)): Request {
    val builder = Request.Builder()
    builder.closure()
    return builder.build()
}

fun Request.Builder.formBody(closure: Body.Builder.() -> Body.Builder)
        : Request.Builder {
    val builder = Body.Builder()
    builder.closure()
    return post(builder.build())
}

fun String.asFile() = File(this)
fun usage() {
    System.err.println("Pemakaian: login-inet logout | <username> <password>")
}

private fun login(username: String, password: String) {
    val client = OkHttpClient()
    val doc = Jsoup.parse(URL("$BASE_URL/login"), 10000)
    if (doc.getElementsByTag("title").text().contains("mikrotik")) {
        println("Anda sudah login.")
        return
    }
    val scripts = doc.getElementsByTag("script")
    var loginScript = scripts.first { it.data().contains("doLogin") }
            .data().reader().readLines().first { it.contains("hexMD5") }
    loginScript = loginScript.substring(loginScript.indexOf("(") + 1, loginScript.indexOf(")"))
    loginScript = loginScript.replace("'", "")
    val tokens = loginScript.split(Regex("[\\\\\\+]")).filter { !it.isNullOrBlank() }.map { it.trim() }
    val comp = ArrayList<Byte>(17 + password.length)
    for (token in tokens) {
        try {
            comp.add(Integer.parseInt(token, 8).toByte())
        } catch (e: NumberFormatException) {
            comp.addAll(password.toByteArray().toList())
        }
    }
    val md5 = DigestUtils.md5Hex(comp.toByteArray())
    val result = Jsoup.parse(client.newCall(request {
        url("$BASE_URL/login")
        formBody {
            add("username", username)
            add("password", md5)
        }
    }).execute().body().string())
    if (result.getElementsByTag("title")[0].text().contains("mikrotik")) {
        println("Anda berhasil login")
    } else if (result.getElementById("esq").text().contains("sessions")) {
        System.err.println("Anda sudah login di tempat lain")
    } else if (result.getElementById("esq").text().contains("salah")) {
        System.err.println("Username/password anda salah dan/atau quota anda habis")
    }
}
fun resolveTilde(path:String):File?{
    var newPath:String
    if(path.startsWith("~/")){
        newPath=path.replaceFirst("~",System.getProperty("user.home"))
        val f=File(newPath)
        if(f.exists())return f
    }else{
        val f=File(path)
        if(f.exists())return f
    }
    return null
}
private val BASE_URL = "http://logout.usd"
fun main(args: Array<String>) {
    if (args.isEmpty()) {
        usage()
        return
    }
    val username: String
    val password: String
    val client = OkHttpClient()
    if (args.size == 1) {
        if (args[0] == "logout") {
            client.newCall(request {
                url("$BASE_URL/logout/")
                get()
            }).execute()
            return
        } else if (resolveTilde(args[0])!=null) {
            val prop = Properties()
            prop.load(resolveTilde(args[0])!!.inputStream())
            if("user" !in prop.stringPropertyNames() || "pass" !in prop.stringPropertyNames()){
                System.err.println("File tidak mengandung definisi user dan/atau password")
                return
            }
            username = prop.getProperty("user")
            password = prop.getProperty("pass")
        } else {
            usage();return
        }
    } else {
        username = args[0]
        password = args[1]
    }
    login(username,password)
}